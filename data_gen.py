
#!pip install import-ipynb
#pip install -U ipykernel

import numpy as np
import torch
import pandas as pd
from torch.utils.data import Dataset
import import_ipynb
import tomita_generator 
from parser_1 import build_parser
import copy
import os
try:
	import cPickle as pickle
except ImportError:
	import pickle as pickle

import json


# import sys
# print(sys.path)

# import importlib
# sys.path.append('c:\\Users\\naa\\Desktop\\BA\\tomita_generator')
# dir(tomita_generator)
# print(sys.path)
# importlib.reload(tomita_generator)

# Dataloader
class TomitaCorpus(object):
    def __init__(self, n, lower_bound, upper_bound, size, unique, leak):
        assert n>0 and n<=7
        L = (upper_bound + lower_bound) // 2
        p = L/ (2 * (L + 1))
        self.leak = leak
        self.unique = unique
        self.Lang = getattr(tomita_generator, 'Tomita{}Language'.format(n))(p,p) 

        self.source, self.target = self.generate_data(size, lower_bound, upper_bound)
        self.nouts = self.Lang.n_letters


    def generate_data(self, size, lower_bound, upper_bound):

        # because of the error of lower bound> upper bound
        if lower_bound >= upper_bound:
            raise ValueError("Lower bound should be less than upper bound")

        ins, outs = self.Lang.training_set_generator(size, lower_bound, upper_bound, self.leak)
        if self.unique:
            ins,outs = zip(*set(zip(ins, outs)))
            ins = list(ins)
            outs = list(outs)

        return ins, outs

def load_data(config, num_bins=2):

    if config.mode == 'train':
        if not config.leak:
            print("Generating Training and Validation Data")
            train_corpus = TomitaCorpus(config.num_par, config.lower_bound, config.upper_bound, config.training_size , unique = True, leak = config.leak)
            #train_corpus = copy.deepcopy(corpus)
            #train_corpus.source, train_corpus.target = train_corpus.source[:config.training_size], train_corpus.target[:config.training_size]
            # val_corpus = copy.deepcopy(corpus)
            # val_corpus.source, val_corpus.target = corpus.source[config.training_size:], corpus.target[config.training_size:]
            val_corpus_bins = []
            # lower_bound = config.upper_bound + 1
            # upper_bound = config.upper_bound + config.len_incr
            lower_bound = config.lower_bound
            upper_bound = config.upper_bound
            for i in range(num_bins):
                print("Generating Data for Lengths [{}, {}]".format(lower_bound, upper_bound))
                val_corpus_bin = TomitaCorpus(config.num_par, lower_bound, upper_bound,  config.test_size, unique = True, leak=config.leak)
                val_corpus_bins.append(val_corpus_bin)
                lower_bound = upper_bound 
                upper_bound = upper_bound + config.len_incr
        else:
            train_corpus = TomitaCorpus(config.num_par, config.lower_bound, config.upper_bound, config.training_size, unique = False, leak = True)
            val_corpus_bins = []
            lower_bound = config.upper_bound 
            upper_bound = config.upper_bound + config.len_incr

            for i in range(num_bins):
                val_corpus_bin = TomitaCorpus(config.num_par, lower_bound, upper_bound, config.test_size, unique = True, leak = True)
                val_corpus_bins.append(val_corpus_bin)	
                    
    print('Training and Validation Data Loaded')

    return train_corpus, val_corpus_bins


def main():
    parser = build_parser()
    args = parser.parse_known_args()
    config= args[0]
    
    # we do this process for each of the 7 languages(each time changing the parser)
    print('Loading data')
    train_corpus, val_corpus_bins = load_data(config, config.bins)
    data_dir = os.path.join('data\MyData', config.dataset)

    if os.path.exists(data_dir) == False:
        os.mkdir(data_dir)

    print("Gathering Length and Depth info of the dataset")

    # print("Writing Train corpus")
    # with open(os.path.join(data_dir, 'train_corpus.pk'), 'wb') as f:
    #     pickle.dump(train_corpus, f)
    #     f.close()
    # print("Done")

    # print("Writing Val corpus bins")
    # with open(os.path.join(data_dir, 'val_corpus_bins.pk'), 'wb') as f:
    #     pickle.dump(file = f, obj = val_corpus_bins)
    # print("Done")

    print("Writing Train text files")
    with open(os.path.join(data_dir, 'train_src.txt'), 'w') as f:
        f.write('\n'.join(train_corpus.source))

    with open(os.path.join(data_dir, 'train_tgt.txt'), 'w') as f:
        f.write('\n'.join(train_corpus.target))
    print("Done")

    print("Writing Val text files")
    for i, val_corpus_bin in enumerate(val_corpus_bins):        
        with open(os.path.join(data_dir, 'val_src_bin{}.txt'.format(i)), 'w') as f:
            f.write('\n'.join(val_corpus_bin.source))

        with open(os.path.join(data_dir, 'val_tgt_bin{}.txt'.format(i)), 'w') as f:
            f.write('\n'.join(val_corpus_bin.target))
    print("Done")

    val_lens_bins, val_depths_bins = [], []
    for i, val_corpus in enumerate(val_corpus_bins):
        val_depths = list(set([val_corpus.Lang.depth_counter(line).sum(1).max() for line in val_corpus.source]))
        val_depths_bins.append(val_depths)

        val_lens = list(set([len(line) for line in val_corpus.source]))
        val_lens_bins.append(val_lens)


    # make the info _dict


    train_depths = list(set([train_corpus.Lang.depth_counter(line).sum(1).max() for line in train_corpus.source]))
    train_lens = list(set([len(line) for line in train_corpus.source]))

    info_dict = {}
    info_dict['Language'] = '{}-{}'.format(config.lang, config.num_par) 
    info_dict['Train Lengths'] = (min(train_lens), max(train_lens))
    info_dict['Train Depths'] = (int(min(train_depths)), int(max(train_depths)))
    info_dict['Train Size'] = len(train_corpus.source)

    for i, (val_lens, val_depths) in enumerate(zip(val_lens_bins, val_depths_bins)):

        if not val_lens or not val_depths:
            continue

        info_dict['Val Bin-{} Lengths'.format(i)] = (min(val_lens), max(val_lens))
        info_dict['Val Bin-{} Depths'.format(i)] = (int(min(val_depths)), int(max(val_depths)))
        info_dict['Val Bin-{} Size'.format(i)] = len(val_corpus_bins[i].source)

    

    with open(os.path.join('data\MyData', config.dataset, 'data_info.json'), 'w') as f:
        json.dump(obj = info_dict, fp = f)

    print("Done with:", config.dataset)

main()
