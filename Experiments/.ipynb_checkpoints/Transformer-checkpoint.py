import torch
import torch.nn as nn
import math 
import numpy as np
import Experiments.Positional_encoding as Positional_encoding
import Experiments.ALiBi as ALiBi
import Experiments.RoPE as RoPE

# This class has the transformer as well as the Encoder and the FeedForward Layer



class EncoderLayer(nn.Module):
    def __init__(self, embedding_dim, inner_layer_dim, norm_size, nhead, dropout, alibi, rope, max_length):
        super().__init__()
        self.layer_norm1 = nn.LayerNorm(norm_size, eps= 0)
        self.layer_norm2 = nn.LayerNorm(norm_size, eps= 0)
        self.alibi = alibi
        self.rope = rope
        if self.alibi:
            self.attention = ALiBi.MultiHeadAtt(embedding_dim, nhead, dropout)
        elif self.rope:
            self.attention = RoPE.RoPEAttnLayer(embedding_dim, nhead ,max_length, dropout)
        else:
            self.attention = nn.MultiheadAttention(embedding_dim, nhead, dropout, batch_first=True)
        self.positionwise_ffn = PositionWiseFeedForward(embedding_dim, inner_layer_dim, dropout)
    def forward(self, x, pad_mask ,mask_attn):
        # apply self_attention mechanism (mask is a boolean)
        # , is_causal= True
        #attention_out = self.attention(x, x, x, pad_mask = pad_mask ,attn_mask=mask_attn, alibi = alibi)
        attention_out, _ = self.attention(x, x, x, key_padding_mask  = pad_mask ,attn_mask=mask_attn)
        #attention_out, _ = self.attention(x, x, x, combine_mask)
        # Add residual connection and attention
        x = x + attention_out
        # apply layer norm
        x_norm = self.layer_norm1(x)
        # apply Positionwise ffn and add to our normalized output attention output
        ffn_out = self.positionwise_ffn.forward(x_norm)
        x = x_norm + ffn_out
        #normalize
        x_norm = self.layer_norm2(x)
        return x_norm

# This is an optional step to create the encoder

def clones(module, N):
    "Produce N identical layers."
    return nn.ModuleList([copy.deepcopy(module) for _ in range(N)])

class Encoder(nn.Module):
    def __init__(self, layer, dropout, num_layers):
        super().__init__()
        self.dropout = nn.Dropout(dropout)
        self.layers = clones(layer, num_layers)
        self.layer_norm = nn.LayerNorm(layer.size)

    def forward(self, x, mask):
        "Pass the input (and mask) through each layer in turn."
        for layer in self.layers:
            x = layer(x, mask)
        return self.layer_norm(x)

# MY transfo
class transfo(nn.Module):
    def __init__(self, embedding_dim , inner_layer_dim, norm_size,  batch_size , dropout , nhead , mask_symbol , n_outputs, max_length, alibi, rope, device, universal, num_layers, pos_enc, no_posi_encoding, layer_number_main):
                
        super().__init__()
        self.mask_symbol = mask_symbol
        self.max_length = max_length

        self.nhead = nhead
        self.device = device
        self.universal = universal
        self.num_layers = num_layers
        self.alibi = alibi
        self.rope= rope
        self.no_posi_encoding = no_posi_encoding
        
        # embedding (dict of 3 and embedding_dim of 32)
        self.embedding = nn.Embedding(3, embedding_dim)
        # choose the positional encoding
        match pos_enc:
            case 'standard':
                self.posi_encoding = Positional_encoding.PositionalEncoding(embedding_dim, dropout, batch_size, self.max_length) 
            case 'cos':
                self.posi_encoding = Positional_encoding.Cos_posi_encoding(embedding_dim, dropout, self.max_length)
            case 'learnable':
                self.posi_encoding = Positional_encoding.Learnable_posi_encoding(embedding_dim, dropout, self.max_length)
            case 'recurrent':
                self.posi_encoding = Positional_encoding.Recurrent_posi_encoding (embedding_dim, dropout, self.max_length)
            case 'learn_period':
                self.posi_encoding = Positional_encoding.Learnable_Period(embedding_dim, dropout, batch_size, self.max_length)  
            case _:
                raise ValueError(f"Unhandled pos_enc value: {pos_enc}")

        # create our enooder layers and stack them (4 per the paper)
        #self.encoder_layer1 = EncoderLayer(embedding_dim, inner_layer_dim, norm_size, nhead, dropout, self.alibi)
        #self.encoder_layer2 = EncoderLayer(embedding_dim, inner_layer_dim, norm_size, nhead, dropout, self.alibi)
        #self.encoder_layer3 = EncoderLayer(embedding_dim, inner_layer_dim, norm_size, nhead, dropout, self.alibi)
        #self.encoder_layer4 = EncoderLayer(embedding_dim, inner_layer_dim, norm_size, nhead, dropout, self.alibi)

        self.encoder_layers = nn.ModuleList([EncoderLayer(embedding_dim, inner_layer_dim, norm_size, nhead, dropout, self.alibi, self.rope, self.max_length) for _ in range(layer_number_main)])
        
        if self.universal:
            self.encoder_universal = EncoderLayer(embedding_dim, inner_layer_dim, norm_size, nhead, dropout, self.alibi, self.rope, self.max_length)
            
        # FCNN
        self.fc = nn.Sequential(nn.LayerNorm(embedding_dim),nn.Linear(embedding_dim, 2 ))
        
    def parameters(self):
        # Return the parameters of the model, including sub-modules
        return super(transfo, self).parameters()
        
    def forward(self, x):
        # create mask first :
        key_padding_mask = pad_mask(x, self.mask_symbol).to(self.device)
        
        attn_mask = torch.ones(self.max_length, self.max_length, dtype = torch.bool)
        attn_mask = torch.logical_not(torch.tril(attn_mask)).to(self.device)
        # embedding
        x = self.embedding(x)

        # if we have alibi or when testing withpout a positional encoding
        if not (self.alibi or self.no_posi_encoding or self.rope):
          x = self.posi_encoding.forward(x)
        #pass x through layers
        if self.universal:
            for i in range(self.num_layers):
                 x = self.encoder_universal.forward(x, key_padding_mask , attn_mask)
        else:  
            #x = self.encoder_layer1.forward(x, key_padding_mask , attn_mask)
            #x = self.encoder_layer2.forward(x, key_padding_mask , attn_mask)
            #x = self.encoder_layer3.forward(x, key_padding_mask , attn_mask)
            #x = self.encoder_layer4.forward(x, key_padding_mask , attn_mask)
            for layer in self.encoder_layers:
                x = layer.forward(x, key_padding_mask, attn_mask)
        # FCNN
        
        x = self.fc(x)
        return x
        

# Padding mask

def pad_mask(input, symbol_to_mask):

    mask = input == symbol_to_mask

    return mask

def loss_mask(target, symbol_to_mask):
    mask_vec = (1-pad_mask(target, symbol_to_mask).float())
    non_masked_vals = torch.sum(mask_vec == 1.0, dim=1)
    return mask_vec, non_masked_vals
    

# https://github.com/ristea/septr/blob/main/septr_block.py (very similar)

class PositionWiseFeedForward(nn.Module):
    def __init__(self, embedding_dim, inner_layer_dim, dropout=0.1):
        super().__init__()
        self.inner_layer_dim = inner_layer_dim
        
        ## make it sequential
        self.layer1 = nn.Linear(embedding_dim, self.inner_layer_dim)
        self.relu= nn.ReLU()
        self.layer2 = nn.Linear(self.inner_layer_dim, embedding_dim)
        self.dropout = nn.Dropout(dropout)
        self.dropout2 = nn.Dropout(dropout)

    def forward(self, x):
        return self.dropout2(self.layer2(self.dropout(self.relu(self.layer1(x)))))