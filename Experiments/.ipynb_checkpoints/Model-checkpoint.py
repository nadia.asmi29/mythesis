import torch
import torch.nn as nn
import math 
import copy
import numpy as np
import matplotlib.pyplot as plt
import Experiments.Pre_process_data as Pre_process_data
import Experiments.Transformer as Transformer
from torch.utils.data import Dataset, DataLoader

# STATISTICS
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score

# Preparing our Dataset class, we implement the len, init and getitem methods
class MyData(Dataset):
    def __init__(self, pos, targets):
        self.pos = pos
        self.targets = targets
    def __len__(self):
        return len(self.pos)
    def __getitem__(self, index):
        return (self.pos[index], self.targets[index])




class run_model():

   def __init__(self, url, learning_rate = 0.001, batch_size= 32, embedding_dim = 32, inner_layer_dim = 128, dropout = 0.1,
                 nheads = 8, mask_symbol = 2 , scheduler_lr = 20, optimizer = 'RMSPROP', split = 0.8, patience = 20, criterion= 'MSE', max_norm = 0.25, alibi = False, rope = False, universal = False, num_layers = 4, pos_enc= 'standard', no_posi_encoding=False, layer_number_main= 4): 
        
        self.batch_size = batch_size
        self.patience = patience
        self.max_norm = max_norm
    	## CREATE MODEL
        max_length, all_instances, targets, all_bin0, targets_bin0, all_bin1, targets_bin1 = Pre_process_data.prep_data(url)
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.model = Transformer.transfo(embedding_dim, inner_layer_dim, embedding_dim, batch_size, dropout, nheads, mask_symbol, 2, max_length, alibi, rope, self.device, universal, num_layers, pos_enc, no_posi_encoding, layer_number_main)

        ## DEFINE LOSS FUNCTION
        match criterion:
           case 'CE':
                self.criterion = nn.CrossEntropyLoss()
           case 'MSE':
                self.criterion = nn.MSELoss(reduction = 'none')

        ## DEFINE OPTIMIZER
        # RMSProp is used in the aper
        match optimizer:
            case 'ADAM':
                self.optimizer = torch.optim.Adam(self.model.parameters(), lr=learning_rate, betas=(0.9, 0.98), eps=1e-9)
            case 'RMSPROP':
                self.optimizer = torch.optim.RMSprop(self.model.parameters(), lr = learning_rate, alpha = 0.99, eps=1e-9)

        ## SCHEDULER
        # TODO: Transformer scheduler instead or remove it 
        self.scheduler = torch.optim.lr_scheduler.StepLR(self.optimizer, step_size=scheduler_lr, gamma=0.1)

        ## SET DEVICE TO GPU IF AVAILABLE
        
        self.model.to(self.device)

        # Load My dataset (for train & eval)

        dataset= MyData(all_instances, targets)
        # split 80% to train and 20% to eval(dev)
        train_size = int(split * dataset.__len__())
        eval_size = dataset.__len__() - train_size
        training_data, eval_data = torch.utils.data.random_split(dataset, [train_size, eval_size])
        # training_data = dataset.sample(frac=0.8, random_state=25)
        # eval_data = dataset.drop(training_data.index)

        # create my train and eval dataloaders

        self.dataloader_train = DataLoader(training_data, batch_size=batch_size, shuffle=True)
        self.dataloader_eval = DataLoader(eval_data, batch_size=batch_size, shuffle=True)

        # prep my data (Test bin0 and bin1)

        dataset_bin0 = MyData(all_bin0, targets_bin0)
        self.dataloader_bin0 = DataLoader(dataset_bin0, batch_size=batch_size, shuffle=True)

        dataset_bin1 = MyData(all_bin1, targets_bin1)
        self.dataloader_bin1 = DataLoader(dataset_bin1, batch_size=batch_size, shuffle=True)

        self.valid_loss = []

   def train_eval(self, num_epochs):
       torch.manual_seed(0)
       np.random.seed(0) 
       # for early stopping
       last_loss = 100
       trigger_times = 0
       # for visualization
       tokenizer = {'0': 0, '1': 1, '#': 2}
       for epoch in range(num_epochs):
           running_loss = 0.0
           running_loss_train = 0.0
           self.model.train()
           total_train = 0
           for i, (data, output) in enumerate(self.dataloader_train):
    
               # Tokenize source and target
               input_tensor = torch.tensor([[tokenizer[char] if char in tokenizer else char for char in seq]for seq in data]).float()
               inputs = input_tensor.to(self.device)
            
               target_tensor = torch.tensor([[tokenizer[char] if char in tokenizer else char for char in seq]for seq in output]).float()
               target_reshaped = Pre_process_data.add_dim_to_tensor(target_tensor)
               target = target_reshaped.to(self.device) 

               # reset the gradients
               self.optimizer.zero_grad()
               
               # get mask for inputs
               mask_vec = (1-Transformer.pad_mask(target, 2).float())
               non_masked_vals = torch.sum(mask_vec == 1.0, dim=1)
               
               # forward 
               outputs = self.model.forward(inputs.long())
               # apply sigmoid to keep the values between 0 and 1
               outputs_sigmoid= torch.sigmoid(outputs)
               
               # SE and Apply mask
               se = self.criterion(target, outputs_sigmoid)
   
               se_masked = se * mask_vec
               # get MSE
               mse = torch.sum(se_masked) / non_masked_vals
               loss_per_batch = mse.mean()
               
               # backward propagation + regularization (clipping) + update optimizer
               loss_per_batch.backward()
               
               torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.max_norm)
               self.optimizer.step()  
               self.scheduler.step() 

           self.model.eval()
           # at Inference we do not calculate grad
           with torch.no_grad():
               correct = 0
               total = 0
               for i, (data, output) in enumerate(self.dataloader_eval):

                   # Tokenize source and target
                   input_tensor = torch.tensor([[tokenizer[char] if char in tokenizer else char for char in seq]for seq in data]).float()
                   inputs = input_tensor.to(self.device)
                   
                   target_tensor = torch.tensor([[tokenizer[char] if char in tokenizer else char for char in seq]for seq in output]).float()
                   target_reshaped = Pre_process_data.add_dim_to_tensor(target_tensor)
                   target = target_reshaped.to(self.device) 
                   
                   # get mask for inputs
                   mask_vec = (1-Transformer.pad_mask(target, 2).float())
                   non_masked_vals = torch.sum(mask_vec == 1.0, dim=1)
               
                   # forward 
                   outputs = self.model.forward(inputs.long())
                   # apply sigmoid to keep the values between 0 and 1
                   outputs_sigmoid= torch.sigmoid(outputs)
                   #reshape my output to match my target shape of (batch_size, max_len, 2)

                   # SE and Apply mask
                   se = self.criterion(target, outputs_sigmoid)
                   se_masked = se * mask_vec
                   # get MSE
                   mse = torch.sum(se_masked) / non_masked_vals
                   loss_per_batch = mse.mean()
                   running_loss += loss_per_batch

                   # Predictions  
                   predicted =  [((out[:n[0]] >= 0.5).float())  for out, n in zip (outputs_sigmoid,  non_masked_vals.long()) ]

                   for j,(tar, pred) in enumerate(zip(target, predicted)):
                        if (tar[:non_masked_vals[j][0]].float() == pred).all():
                            correct += 1
                   total += target.size(0)
           accuracy = 100 * correct / total
           loss = running_loss / total
           self.valid_loss.append(loss)
           print(f'Epoch [{epoch+1}/{num_epochs}], Loss: {loss:.6f}, dev Accuracy: {accuracy:.2f}%')
           
           #early stopping
           if loss > last_loss:
                trigger_times +=1
                if trigger_times >= self.patience:
                    print('early stopping at', epoch+1)
                    return 
           else:
                trigger_times = 0
           last_loss = loss

   def test(self, bin):  
       with torch.no_grad():
        torch.manual_seed(0)
        correct = 0
        total = 0
        if bin == 0:
            dataloader = self.dataloader_bin0
            msg = 'all bin 0'
        elif bin == 1:
            dataloader = self.dataloader_bin1
            msg = 'all bin 1'
        tokenizer = {'0': 0, '1': 1, '#': 2}
        for data, output in dataloader:

            # Tokenize source and target
            input_tensor = torch.tensor([[tokenizer[char] if char in tokenizer else char for char in seq]for seq in data]).float()
            inputs = input_tensor.to(self.device)
            
            target_tensor = torch.tensor([[tokenizer[char] if char in tokenizer else char for char in seq]for seq in output]).float()
            target_reshaped = Pre_process_data.add_dim_to_tensor(target_tensor)
            target = target_reshaped.to(self.device) 
            
            # get mask for inputs
            mask_vec = (1-Transformer.pad_mask(target, 2).float())
            non_masked_vals = torch.sum(mask_vec == 1.0, dim=1)
               
            # forward 
            outputs = self.model.forward(inputs.long())
            # apply sigmoid to keep the values between 0 and 1
            outputs_sigmoid= torch.sigmoid(outputs)
            #reshape my output to match my target shape of (batch_size, max_len, 2)
            #outputs_sigmoid = add_dim_to_tensor(outputs_sigmoid)
            # Predictions 
            predicted =  [((out[:n[0]] >= 0.5).float())  for out, n in zip (outputs_sigmoid,  non_masked_vals.long()) ]

            for j,(tar, pred) in enumerate(zip(target, predicted)):
                if (tar[:non_masked_vals[j][0]].float() == pred).all():
                    correct += 1
            total += target.size(0)

        accuracy = 100 * correct / total
        print('accuracy of', msg,  'is: ', accuracy ,'%')  
        return accuracy

   def get_losses(self, name):
       valid_loss_tensor = torch.tensor(self.valid_loss).cpu()
       valid_loss = valid_loss_tensor.numpy()
       return valid_loss, name 
       
   def visualize(self, num_epochs):
       valid_loss_tensor = torch.tensor(self.valid_loss).cpu()
       valid_loss = valid_loss_tensor.numpy()
       plt.figure(figsize=(10, 5))
       plt.plot(np.arange(1, num_epochs + 1), valid_loss, label='Validation Loss', color='red')
       plt.xlabel('Epochs')
       plt.ylabel('Loss')
       plt.title('Loss Curve')
       plt.legend()
       plt.show()
