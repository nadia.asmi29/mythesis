import os


# This Class has the preprocessing methods
def min_max_stats(url):
    pos_instances,next_steps,pos_instances_dev_1,next_steps_dev_1,pos_instances_dev_2,next_steps_dev_2 = load_data(url)
    min_train_src= get_min(pos_instances)
    max_train_src= get_max(pos_instances)
    min_train_tgt = get_min(next_steps)
    max_train_tgt = get_max(next_steps)
    min_bin0_src = get_min(pos_instances_dev_1)
    max_bin0_src = get_max(pos_instances_dev_1)
    min_bin0_tgt = get_min(next_steps_dev_1)
    max_bin0_tgt = get_max(next_steps_dev_1)
    min_bin1_src = get_min(pos_instances_dev_2)
    max_bin1_src = get_max(pos_instances_dev_2)
    min_bin1_tgt = get_min(next_steps_dev_2)
    max_bin1_tgt = get_max(next_steps_dev_2)
    return [min_train_src, max_train_src], [min_train_tgt, max_train_tgt], [min_bin0_src, max_bin0_src], [min_bin0_tgt, max_bin0_tgt], [min_bin1_src, max_bin1_src], [min_bin1_tgt, max_bin1_tgt]
def stats(url):
    # LOAD all of our data 
    pos_instances, next_steps, pos_instances_dev_1, next_steps_dev_1 , pos_instances_dev_2, next_steps_dev_2 = load_data(url)
    max_length = max(get_max(pos_instances),get_max(pos_instances_dev_1),get_max(pos_instances_dev_2))
    size_train_src= len(pos_instances)
    size_train_tgt = len(next_steps)
    size_bin0_src = len(pos_instances_dev_1)
    size_bin0_tgt = len(next_steps_dev_1)
    size_bin1_src = len(pos_instances_dev_2)
    size_bin1_tgt = len(next_steps_dev_2)
    return max_length, size_train_src, size_train_tgt, size_bin0_src, size_bin0_tgt, size_bin1_src, size_bin1_tgt

def stats_avg(url):
    # LOAD all of our data 
    pos_instances, next_steps, pos_instances_dev_1, next_steps_dev_1 , pos_instances_dev_2, next_steps_dev_2 = load_data(url)
    max_length = max(get_max(pos_instances),get_max(pos_instances_dev_1),get_max(pos_instances_dev_2))
    avg_train_src= get_avg(pos_instances)
    avg_train_tgt = get_avg(next_steps)
    avg_bin0_src = get_avg(pos_instances_dev_1)
    avg_bin0_tgt =get_avg(next_steps_dev_1)
    avg_bin1_src = get_avg(pos_instances_dev_2)
    avg_bin1_tgt = get_avg(next_steps_dev_2)
    return max_length, avg_train_src, avg_train_tgt, avg_bin0_src, avg_bin0_tgt, avg_bin1_src, avg_bin1_tgt
    
def load_data(url):
    # load train data
    pos_instances = open(os.path.join(url,'train_src.txt'), encoding='utf8').read().split('\n')
    next_steps = open(os.path.join(url,'train_tgt.txt'), encoding='utf8').read().split('\n')

    # load test data bin 0
    pos_instances_dev_1 = open(os.path.join(url,'val_src_bin0.txt'), encoding='utf8').read().split('\n')
    next_steps_dev_1 = open(os.path.join(url,'val_tgt_bin0.txt'), encoding='utf8').read().split('\n')

    # load test data bin 1
    pos_instances_dev_2 = open(os.path.join(url,'val_src_bin1.txt'), encoding='utf8').read().split('\n')
    next_steps_dev_2 = open(os.path.join(url,'val_tgt_bin1.txt'), encoding='utf8').read().split('\n')

    return  pos_instances, next_steps, pos_instances_dev_1, next_steps_dev_1 , pos_instances_dev_2, next_steps_dev_2


# Get the maximum token length of both pos instances and the valid next steps
# in our case our language has only two possible alphabets, so the length of the output is 2x len of input
def get_max(pos):
    max_pos = 0
    for i in pos:
        if len(i)> max_pos:
            max_pos = len(i)  
    return max_pos

def get_min(pos):
    min_pos = 200
    for i in pos:
        if len(i)< min_pos:
            min_pos = len(i)  
    return min_pos


def get_avg(pos):
     average_length = sum(len(sequence) for sequence in pos) / len(pos)
     return average_length
    
# Padding : here we pad using a neutral token that we will mask later (*, 2, #, A )
def padding(max_length, words):
  padded = []
  for i in words:
    if len(i)< max_length:
        pad = i.ljust(max_length, '#')
        #pad = i + pad [:(max_length-len(i))]
        padded.append(pad)
    else:
        padded.append(i)
  return padded

#### Only when I transform my input seq to 3D
def one_hot_encode_3_dim(pos_seq):
     # define vocab
    vocab = ['01', '10']
    # init one-hot encodings list for all sequences
    one_hot_encodings = []
    for seq in pos_seq:
        # init a list of one Hot encoding for each sequence
        seq_hot_encoded = []
        for char in seq:
            match char:
                case '0':
                    seq_hot_encoded.append('10')
                case '1':
                    seq_hot_encoded.append('01')
                    
        one_hot_encodings.append(seq_hot_encoded)
    return one_hot_encodings


def one_hot_encode(pos_seq):
    # define vocab
    vocab = ['0', '1']
    # init one-hot encodings list for all sequences
    one_hot_encodings = []
    for seq in pos_seq:
        # init a list of one Hot encoding for each sequence
        seq_hot_encoded = ''
        for char in seq:
            match char:
                case '0':
                    seq_hot_encoded = seq_hot_encoded + '10'
                case '1':
                    seq_hot_encoded = seq_hot_encoded + '01'
                    
        one_hot_encodings.append(seq_hot_encoded)
    return one_hot_encodings

def add_dim_to_tensor(input):
   input_reshaped = input.reshape(input.shape[0], -1, 2)
   return input_reshaped


def prep_data(url, padding_flag = True, third_dim = False):
    # LOAD all of our data 
    pos_instances, next_steps, pos_instances_dev_1, next_steps_dev_1 , pos_instances_dev_2, next_steps_dev_2 = load_data(url)
     # get maximum length between all positive instances (train, dev and test) for the padding process
    max_length = max(get_max(pos_instances),get_max(pos_instances_dev_1),get_max(pos_instances_dev_2))
    if padding_flag:
        # PAD
        # Pad my train dataset
        padded_pos_instances = padding(max_length, pos_instances)
        padded_next_steps = padding(max_length * 2, next_steps)
        # check if my padding is correct
        #for i in padded_pos_instances:
        #    if len(i)< 400:
        #        raise Exception("My Padding is wrong, wah wah!")
        
        # Pad my dev datasets
        padded_pos_dev_1 = padding(max_length, pos_instances_dev_1)
        padded_next_steps_dev_1 = padding(max_length * 2, next_steps_dev_1)

        # Pad my test dataset
        padded_pos_dev_2 = padding(max_length, pos_instances_dev_2)
        padded_next_steps_dev_2 = padding(max_length * 2, next_steps_dev_2)

    return max_length, padded_pos_instances, padded_next_steps, padded_pos_dev_1, padded_next_steps_dev_1, padded_pos_dev_2, padded_next_steps_dev_2

