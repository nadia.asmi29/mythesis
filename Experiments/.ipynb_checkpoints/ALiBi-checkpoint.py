import torch
import torch.nn as nn
import math 
import numpy as np
from labml.logger import inspect
from labml_nn.transformers.mha import MultiHeadAttention

#This class has the custom Attention head that I created to integrate ALiBi into the standard MultiheadAttention

# This part is for alibi
# inspired by https://github.com/ofirpress/attention_with_linear_biases/blob/master/fairseq/models/transformer.py#L949
# I only use heads that are divisible by 2 anyways, if I want to use another number of heads I need to adapt my code

def get_slopes(nhead):
  init = (2**(-2**-(math.log2(nhead)-3)))
  ratio = init
  return [init*ratio**i for i in range(nhead)]

def prep_alibi_1(nhead, max_len, batch_size):
  slopes = torch.Tensor(get_slopes(nhead)).unsqueeze(1).unsqueeze(1)
  index = torch.arange(max_len).unsqueeze(0).unsqueeze(0).expand(nhead, -1, -1)
  alibi = (slopes * index)
  alibi = alibi.repeat(batch_size, 1, 1)
  print(alibi.shape)
  return alibi
  #return alibi
def prep_alibi(max_len, nhead, batch_size):
    slopes = torch.Tensor(get_slopes(nhead)).unsqueeze(1).unsqueeze(1)
    index = torch.arange(max_len)

    # Create an empty tensor to store the distances
    index_tensor = torch.empty((100, 100))

    # Fill the shifted_tensor with shifted rows
    for i in range(100):
        index_tensor[i] = (index - i) 
    alibi = index_tensor.unsqueeze(0).expand(nhead, -1,-1)
    alibi = (slopes * alibi)
    alibi = alibi.unsqueeze(1)
    alibi = alibi.repeat(1, batch_size, 1,1)
    return alibi.permute(3,2,1,0)
    # we need this shape `[seq_len_q, seq_len_k, batch_size, heads]`





# Code inspired from the annotated implementation nn.labml.ai
#https://github.com/labmlai/annotated_deep_learning_paper_implementations/blob/master/labml_nn/transformers/mha.py

class MultiHeadAtt(nn.Module):
    def __init__(self, embedding_dim, nhead, dropout):
        super().__init__()
        self.d_k = embedding_dim // nhead
        self.nhead = nhead
        self.attn = None
        self.embedding_dim = embedding_dim
        
        self.W_q = nn.Linear(embedding_dim, nhead * self.d_k)
        self.W_k = nn.Linear(embedding_dim, nhead * self.d_k)
        self.W_v = nn.Linear(embedding_dim, nhead * self.d_k)
        self.W_o = nn.Linear(embedding_dim, embedding_dim)

        self.dropout = nn.Dropout(dropout)
        self.softmax = nn.Softmax(dim=1)
        
    def calculate_scores(self, query, key):
        result = torch.matmul(query.permute(1, 2, 0, 3), key.permute(1, 2, 3, 0))
        result = result.permute(2,3,0,1)
        # torch.einsum('ijkh, bjkh ->ibjk', query, key)
        return result
        
    def reshape_tensor(self, x, projection):
        # gives us the shape of the tensor except the last dimension(ex: tensor (2,3,4) => we get (2,3))
        x = x.permute(1,0,2)
        shape_x = x.shape[:-1] 
        # project x onto linear dense layer
        x = projection(x)
        # now we get a shape of (max_len, batch_size, nhead *d_k ), we need to split the last 2
        return x.view(*shape_x, self.nhead, self.d_k)
        
    def forward(self, query, key, value, key_padding_mask= None, attn_mask= None, alibi= True):
        # query,key,value of shape (batch_size, max_len, embedding_dim) to become (max_len, batch_size, nhead ,d_k)
        batch_size, max_len, _ = query.shape
        query = self.reshape_tensor(query, self.W_q)
        key = self.reshape_tensor(key, self.W_k)
        value = self.reshape_tensor(value, self.W_v)

        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        #[max_len, max_len, batch_size, nhead]
        scores = self.calculate_scores(query, key).to(device)
        if alibi:
            # my alibi is of shape (max_len, max_len, batch_size,  nhead)
            alibi_biases = prep_alibi(max_len, self.nhead, batch_size).to(device)
            scores = scores - alibi_biases
        # scaling 
        scores = scores / math.sqrt(torch.tensor(self.d_k, dtype=torch.float32))

        # TODO: apply padding mask unsqueeze(0) or unsqueeze(1), for nhead unsqueeze(3): try to broadcast 
        #apply mask
        mask = None
        if key_padding_mask is not None:
            key_padding_mask = key_padding_mask.permute(1,0)
            key_padding_mask = key_padding_mask.unsqueeze(0).unsqueeze(3)
            key_padding_mask = key_padding_mask.repeat(100, 1, 1, self.nhead)
            mask = key_padding_mask
        if attn_mask is not None:
            #reshape mask from(max_len, max_len) to (max_len,max_len, batch_size, nhead)
            attn_mask = attn_mask.unsqueeze(-1).unsqueeze(-1)
            attn_mask = attn_mask.repeat(1,1,batch_size, self.nhead)
            if mask is not None:
                mask = torch.logical_or(mask, attn_mask)
            else:
                mask = attn_mask
            scores = scores.masked_fill(mask, float('-inf'))

        
        # softmax + dropout
        self.attn = self.dropout(self.softmax(scores))
        # we get this shape (max_len, max_len, batch_size, nhead)

        #multiply with value
        self.attn = torch.einsum("ijbh,jbhd->bihd", self.attn, value)
        # concatenate, we get (batch_size, max_len, embedding_dim)
        x = self.attn.reshape(batch_size, max_len, -1)
        # last projection of output layer
        x = self.W_o(x)

        return x, scores
        