import torch
import torch.nn as nn
import math 
import numpy as np

# This class has the following Positional Encoding classes 
# - Standard Sin/cos
# - Cos
# - Learnable
# - Recurrent
# - learnable Period
# - No Positional Encoding is inside of the Tranformer



# based on/inspired by the Class PositionalEncoding from the paper Attention is all you need, with a change in dimensions
# Vasmari et al use these functions to create a instanceof position-specific values
#PE(pos,2i) = sin(pos/10000^(2i/model_dim))
#PE(pos, 2i+1) = cos(pos/10000^(2i/model_dim))
# where pos: order in the sentence(position) and i: position along the embedding vector dimension (dimension)
# link : https://github.com/harvardnlp/annotated-transformer/blob/master/AnnotatedTransformer.ipynb

class PositionalEncoding(nn.Module):
    def __init__(self, embedding_dim, dropout, batch_size, max_len):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout) 
        self.pe= torch.zeros(max_len, embedding_dim)
        self.max_len = max_len
        self.batch_size = batch_size
        self.embedding_dim = embedding_dim
        
        pos = torch.arange(end=max_len).unsqueeze(1)
        two_i_term = torch.arange(start=0, end=embedding_dim, step=2) 
        bottom_term = torch.pow(10000, two_i_term / embedding_dim )
        
        # start at 0 skip 2 => gets all evens [0, 2, ...]
        self.pe[:, 0::2] = torch.sin(pos / bottom_term)
        # start at 1 skip 2 => gets all odds [1, 3, ...]
        self.pe[:, 1::2] = torch.cos(pos / bottom_term)
        
        #add batch_size dimention
        self.pe = self.pe.unsqueeze(0)
        self.pe = self.pe.repeat(batch_size, 1, 1)
        
    def forward(self, x):
        # to fix runtime error may add .to(x.device) to be sure the operation is made with both tensors
        # on CPU or both on GPU
        x = x + self.pe[:x.shape[0], :x.shape[1], :].to(x.device)
        return self.dropout(x)



class Cos_posi_encoding (nn.Module):
    def __init__(self, embedding_dim, dropout, max_len):
        super().__init__()
        
        self.dropout = nn.Dropout(dropout)
        self.pe= torch.ones(max_len, embedding_dim)
        for i in range(max_len):
            self.pe[i] = self.pe[i] * math.cos(i * math.pi)
        self.pe = self.pe.unsqueeze(0).transpose(0, 1)
		#self.register_buffer('pe', pe)

    def forward(self, x):
        x = x + self.pe[:x.size(0), :].to(x.device)
        return self.dropout(x)




# source, based on/inspired by : https://nn.labml.ai/transformers/vit/index.html & 
# https://github.com/google-research/vision_transformer

class Learnable_posi_encoding(nn.Module):
    def __init__(self, embedding_dim, dropout, max_len = 5_000):
        super().__init__()
        self.dropout = nn.Dropout(dropout)
        
        self.positional_encodings = nn.Parameter(torch.zeros(max_len, 1, embedding_dim), requires_grad=True)
        
    def forward(self, x):
        pe = self.positional_encodings[:x.shape[0]]
        x += pe
        return self.dropout(x)

class Recurrent_posi_encoding(nn.Module):
    def __init__(self, embedding_dim, dropout, max_len, num_rec_layers=2):
        super().__init__()
        self.dropout = nn.Dropout(dropout)
        self.embedding_dim = embedding_dim
        self.max_len = max_len
        self.rnn = nn.RNN(input_size=embedding_dim, hidden_size=embedding_dim, num_layers=num_rec_layers)
        
    def forward(self, x):
        posi_vector = nn.Parameter(torch.rand(self.embedding_dim, device = x.device))
        posi_encoding= posi_vector.unsqueeze(0).repeat(self.max_len, 1)
        # Shape is (max_len * embedding_dim) the batch_size will be broadcast by default
        output, _ = self.rnn(posi_encoding)
        x += output
        return self.dropout(x)



# uses the Sin/Cos with learnable Period
class Learnable_Period(nn.Module):
    def __init__(self, embedding_dim, dropout, batch_size, max_len):
        super().__init__()
        self.dropout = nn.Dropout(p=dropout) 
        self.pe= torch.zeros(max_len, embedding_dim)
        self.max_len = max_len
        self.batch_size = batch_size
        self.embedding_dim = embedding_dim

        self.freqs = nn.Parameter(torch.ones(embedding_dim) * 0.01)
        
        pos = torch.arange(end=max_len).unsqueeze(1)
        two_i_term = torch.arange(start=0, end=embedding_dim, step=2) 
        bottom_term = torch.pow(1000, two_i_term / embedding_dim )
        
        # start at 0 skip 2 => gets all evens [0, 2, ...]
        self.pe[:, 0::2] = torch.sin(pos / bottom_term)
        # start at 1 skip 2 => gets all odds [1, 3, ...]
        self.pe[:, 1::2] = torch.cos(pos / bottom_term)
        
        #add batch_size dimention
        self.pe = self.pe.unsqueeze(0)
        self.pe = self.pe.repeat(batch_size, 1, 1)
        
    def forward(self, x):
        # to fix runtime error may add .to(x.device) to be sure the operation is made with both tensors
        # on CPU or both on GPU
        x = x + self.pe[:x.shape[0], :x.shape[1], :].to(x.device) * self.freqs.to(x.device)
        return self.dropout(x)

















    