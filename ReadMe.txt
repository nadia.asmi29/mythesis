
README for Your Project

This is just a small walkthough over the code, I believe the code is understandable enough the way it is structured and some important comments are added too.
I have 6 parts to my code:
1) CODE GENERATION: is done by running data_gen.py through scripts and giving it your needed args. I also uncluded the data that I used and it is found in Data of course: The data is segmented to 6 Datasets per Language and there are 7 Languages. For each one, we have training sets, and two testing sets. For each of those sets, we have targets and sources; The source being the input. The difference between the 2 testing sets is length. The dataset bin0 samples data of similar length limits as our training data, while bin1 includes longer sequences. 
#######################################################################################################################
2) PREPROCESSIJNG: that includes loading and padding, clipping if necessary. Transformations to one hot Encodings are also possible if you want to work with that type of embedding. For my reported training I kept it simple as I did not see any improvement in performance from all the transformations.
All the needed methods are within class Pre_process_data.py
#######################################################################################################################
3) TRAINING: We have a Transformer class, this is where the combination of your specific Transformer is done. So wether you choose to only enclude encoder layers, or only decoder or both ect..
#######################################################################################################################
4) After building the Transformer we move to Model.py. And this of course is where the model is created. This is where the type of positional encoding, found in Positonal_encoding.py, the type of attention, found in ALiBi.py or in RoPE.py, can be chosen. You can give it directly as a arg. 
#######################################################################################################################
5)CHOOSE MODEL: for the running of the training as well as the visualization and so on. It is just a personal choice as I think it is more confortable.
######################################################################################################################
6) NOTEBOOK EXPERIMENTS: but only kept the relevant notebooks. So I have 7 Tomita classes. Each runs a multitude of experimenets and combinations on the respective languge. 


