
#!pip install argparse

import argparse

def build_parser():

    parser = argparse.ArgumentParser(description= 'Run Single seq model')

    # mode Specifications
    
    parser.add_argument('-mode', type=str, default='train', choices=['train', 'test'], help='Modes: train, test')
    parser.add_argument('-debug', dest='debug', action='store_true', help='Operate in debug mode')

    ## data_gen params

    parser.add_argument('-lang', type=str, default= 'Tomita1', choices=  ['Tomita1', 'Tomita2', 'Tomita3', 'Tomita4', 'Tomita5', 'Tomita6', 'Tomita7'], help='Formal Language')
    parser.add_argument('-lower_bound', type=int, default=2, help='Lower Length Window')
    parser.add_argument('-upper_bound', type=int, default=50, help='Upper Length Window')
    parser.add_argument('-training_size', type=int, default=10000, help='Training data size')
    parser.add_argument('-test_size', type=int, default=2000, help='Test data size')
    parser.add_argument('-leak', dest='leak', action='store_true', help='leak Data')
    parser.add_argument('-num_par', type=int, default=5, help='Dyck-n or abc or Tomita_n...')
   
    
    parser.add_argument('-bins', type=int, default=2, help='Number of validation bins')
    parser.add_argument('-dataset', type=str, choices= ['Tomita1', 'Tomita2', 'Tomita3', 'Tomita4', 'Tomita5', 'Tomita6', 'Tomita7'], default='Tomita1', help='Dataset of the 7 Tomita languages')
    parser.add_argument('-len_incr', type = int, default = 50)

    # device config

    parser.add_argument('-gpu', type=int, default=1, help='Specify the gpu to use')
    parser.add_argument('-seed', type=int, default=1729, help='Default seed to set')
    parser.add_argument('-logging', type=int, default=1, help='Set to 0 if you do not require logging')
    parser.add_argument('-ckpt', type=str, default='model', help='Checkpoint file name')

    parser.parse_args()

    return parser




